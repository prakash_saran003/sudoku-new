//events - a super-basic Javascript (publish subscribe) pattern

class Event {
  constructor() {
    this.events = {};
  }

  on(eventName, fn) {
    this.events[eventName] = fn;
    // this.events[eventName] = this.events[eventName] || [];
    // this.events[eventName].push(fn);
  }

  // off(eventName, fn) {
  //   if (this.events[eventName]) {
  //     for (var i = 0; i < this.events[eventName].length; i++) {
  //       if (this.events[eventName][i] === fn) {
  //         this.events[eventName].splice(i, 1);
  //         break;
  //       }
  //     }
  //   }
  // }

  trigger(eventName, data) {
    // console.log("[event.js] this.events : ", this.events);
    // console.log("[event.js] eventName : ", eventName);
    // console.log("[event.js] data : ", data);
    if (this.events[eventName]) {
      this.events[eventName](data);
    }
  }
}

export default new Event();

const EASY_SCORE = 600;
const HARD_SCORE = 1200;
const MEDIUM_SCORE = 900;
const EXPERT_SCORE = 1800;
const APP_NAME = "Sudoku Saint";
const MY_MAIL = "rpapps25@gmail.com";
const PACKAGE_NAME = "com.rpapps.sudoku";
const MAIL_SUBJECT = APP_NAME + " feedback";
const MUSICFOLDER = "myMusic";
const HINT = "hint";
const TOKEN = "token";
const SECOND_CHANCE = "second-chance";
const GAME_TYPE = {
  NAME: "game-type",
  CLASSIC: "classic",
  LIGHTENING: "lightening",
};
const GAME_SUB_TYPE = {
  NAME: "game-sub-type",
  EASY: "easy",
  MEDIUM: "medium",
  HARD: "hard",
  EXPERT: "expert",
};
const GAME_NEW_RESUME_TYPE = {
  NAME: "game-new-resume-type",
  NEW_GAME: "new-game",
  RESUME: "resume",
};
const NOTIFICATION_CHANNEL = {
  id: "NEW_MUSIC_ADDED",
  description: "New Music added.",
  name: "NEW MUSIC ADDED",
  sound: "notification_sound",
  vibration: true,
  light: true,
  lightColor: parseInt("FF0000FF", 16).toString(),
  importance: 4,
  badge: true,
  visibility: 1,
};
const PENCIL_FILL_ALL = "pencil-fill-all";
const STOP_TIMER = "stop-timer";
const SHOW_SPECIFIC_NUM = "show-specific-num";

exports.install = function(Vue) {
  Vue.constants = {
    HINT,
    TOKEN,
    MY_MAIL,
    APP_NAME,
    GAME_TYPE,
    EASY_SCORE,
    HARD_SCORE,
    STOP_TIMER,
    MUSICFOLDER,
    EXPERT_SCORE,
    MAIL_SUBJECT,
    MEDIUM_SCORE,
    PACKAGE_NAME,
    GAME_SUB_TYPE,
    SECOND_CHANCE,
    PENCIL_FILL_ALL,
    SHOW_SPECIFIC_NUM,
    GAME_NEW_RESUME_TYPE,
    NOTIFICATION_CHANNEL,
  };
};

module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	// list here all supported plugins
	var pluginsList = [
		'cordova-admob',
		'cordova-plugin-device',
		'cordova-plugin-network-information',
		'cordova-plugin-x-toast',
		'cordova-plugin-background-mode',
		'cordova-plugin-theme-detection',
	];

	exports.install = function (Vue, options) {

	  // declare global Vue.cordova object
	  Vue.cordova = Vue.cordova || {
	    deviceready: false,
	    plugins: []
	  };

	  // Cordova events wrapper
	  Vue.cordova.on = function (eventName, cb) {
	    document.addEventListener(eventName, cb, false);
	  };

	  // let Vue know that deviceready has been triggered
	  document.addEventListener('deviceready', function () {
	    Vue.cordova.deviceready = true;
	  }, false);

	  // load supported plugins
	  pluginsList.forEach(function (pluginName) {
	    var plugin = __webpack_require__(1)("./" + pluginName);
	    plugin.install(Vue, options, function (pluginLoaded) {
	      if (pluginLoaded) {
	        Vue.cordova.plugins.push(pluginName);
	      }
	      if (Vue.config.debug) {
	        console.log('[VueCordova]', pluginName, '→', pluginLoaded ? 'loaded' : 'not loaded');
	      }
	    });
	  });
	};

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./cordova-admob": 2,
		"./cordova-admob.js": 2,
		"./cordova-plugin-device": 3,
		"./cordova-plugin-device.js": 3,
		"./cordova-plugin-network-information": 4,
		"./cordova-plugin-network-information.js": 4,
		"./cordova-plugin-x-toast": 5,
		"./cordova-plugin-x-toast.js": 5,
		"./cordova-plugin-background-mode": 6,
		"./cordova-plugin-background-mode.js": 6,
		"./cordova-plugin-theme-detection": 7,
		"./cordova-plugin-theme-detection.js": 7,
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 1;


/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';

	exports.install = function (Vue, options, cb) {
		document.addEventListener('deviceready', function () {

			if (typeof window.admob === 'undefined') {
				return cb(false);
			}

			// pass through the camera object
			Vue.cordova.admob = window.admob;

			return cb(true);
		}, false);
	};

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';

	exports.install = function (Vue, options, cb) {
	  document.addEventListener('deviceready', function () {

	    if (typeof device === 'undefined') {
	      return cb(false);
	    }

	    // pass through the contacts object
	    Vue.cordova.device = device;

	    return cb(true);
	  }, false);
	};

/***/ },
/* 4 */
/***/ function(module, exports) {

	'use strict';

	exports.install = function (Vue, options, cb) {
	  document.addEventListener('deviceready', function () {

			if (typeof navigator.connection === 'undefined') {
	      return cb(false);
	    }

	    // pass through the geolocation object
	    Vue.cordova.connection = navigator.connection;

	    return cb(true);
	  }, false);
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	exports.install = function (Vue, options, cb) {
	  document.addEventListener('deviceready', function () {

			if (typeof window.plugins.toast === 'undefined') {
	      return cb(false);
	    }

	    // pass through the geolocation object
	    Vue.cordova.toast = window.plugins.toast;

	    return cb(true);
	  }, false);
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';

	exports.install = function (Vue, options, cb) {
	  document.addEventListener('deviceready', function () {

			if (typeof cordova.plugins.backgroundMode === 'undefined') {
	      return cb(false);
	    }

	    // pass through the geolocation object
	    Vue.cordova.backgroundMode = cordova.plugins.backgroundMode;

	    return cb(true);
	  }, false);
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';

	exports.install = function (Vue, options, cb) {
	  document.addEventListener('deviceready', function () {

			if (typeof cordova.plugins.ThemeDetection === 'undefined') {
	      return cb(false);
	    }

	    // pass through the geolocation object
	    Vue.cordova.ThemeDetection = cordova.plugins.ThemeDetection;

	    return cb(true);
	  }, false);
	};

/***/ },
/******/ ]);